//
//  ViewController.swift
//  diceapp
//
//  Created by iulian david on 7/23/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import UIKit

let startDiceAssetName = "dice"
class ViewController: UIViewController {

    @IBOutlet weak var dice2: UIImageView!
    @IBOutlet weak var dice1: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setRandomImage(for: dice1)
        setRandomImage(for: dice2)
    }

    fileprivate func updateDices() {
        setRandomImage(for: dice1)
        setRandomImage(for: dice2)
    }
    
    @IBAction func roll(_ sender: Any) {
        
        updateDices()
        
    }
    
    private func setRandomImage(for imageView: UIImageView) {
        let dice = Int(arc4random_uniform(6)) + 1
        UIView.animate(withDuration: 0.3) {
            imageView.image = UIImage(named: "\(startDiceAssetName)\(dice)")
        }
    }
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        guard motion == .motionShake else {
            return
        }
        
        updateDices()
    }
}

